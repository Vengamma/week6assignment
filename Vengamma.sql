create database balaji;

use balaji

create table moviescoming(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4));

insert into moviescoming values(1,'superman','action',2018);
insert into moviescoming values(2,'marval','romantic',2017);
insert into moviescoming values(3,'tiger','Drama',2012);
insert into moviescoming values(4,'spiderman','Thriller',2010);
insert into moviescoming values(5,'heaman','action',2017);

select * from moviescoming;

create table moviesintheater(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4));

insert into moviesintheater values(1,'iron man','action',2017);
insert into moviesintheater values(2,'captian','Drama',2018);
insert into moviesintheater values(3,'dangal','action',2017);
insert into moviesintheater values(4,'jersey','romantic',2014);
insert into moviesintheater values(5,'harypotter','action',2012);

select * from moviesintheater;

create table topratingmovies(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4));


insert into topratingmovies values(1,'iron man','action',2013);
insert into topratingmovies values(2,'apider man','Drama',2014);
insert into topratingmovies values(3,'spiderman','romantic',2014);
insert into topratingmovies values(4,'pk','Thriller',2017);
insert into topratingmovies values(5,'bommar','romantic',2018);

select * from topratingmovies;

create table topratingindian(
id int(5) primary key,
title varchar(20),genres varchar(20),year int(4));

insert into topratingindian values(1,'roja','action',2017);
insert into topratingindian values(2,'good','romantic',2017);
insert into topratingindian values(3,'mayabazar','Drama',2017);
insert into topratingindian values(4,'lifeofpi','action',2017);
insert into topratingindian values(5,'sea','romantic',2017);
select * from topratingindian;
