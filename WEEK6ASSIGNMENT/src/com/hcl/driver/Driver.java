package com.hcl.driver;


import java.sql.SQLException;
import java.util.Scanner;

import javax.management.NotificationFilter;

import com.hcl.Interfaces.MovieComing;
import com.hcl.Interfaces.MoviesinTheatres;
import com.hcl.Interfaces.Notification;
import com.hcl.Interfaces.NotificationFactory;
import com.hcl.Interfaces.TopRatedIndia;
import com.hcl.Interfaces.TopRatedMovies;
import com.hcl.Model.Movie;


public class Driver {
	@SuppressWarnings("resource")
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("choose 1 to display the movie");
			System.out.println("choose 2 to add the movie to movie coming");
			System.out.println("choose 3 to add the movie to movie in theater");
			System.out.println("choose 4 to add the movie to the top rated indian movie");
			System.out.println("choose 5 to add the movie to the top rated movie");
                        System.out.println("choose 6 to exit");
                        Movie movie = new Movie();
			int value = sc.nextInt();
			switch (value) {

			case 1:

				System.out.println(" choose the following :moviescoming ,moviesinTheater,TopRatedIndian,topRatedMovies");
				String chosen = sc.next();
				NotificationFactory notificationFactory = new NotificationFactory();
				Notification notification = notificationFactory.createNotification(chosen);
				System.out.print(notification.getMovies());
				break;

			case 2:
				// adding into coming soon list

				movie.setId(4);
				movie.setTitle("Mars");
				movie.setCategory("coming soon");
				movie.setYear(2009);
				MovieComing movc = new MovieComing();

				// add
				System.out.println(movc.add(movie) + "added into database");
				break;

			case 3:
				// adding into movie in theater list
				movie.setId(4);
				movie.setTitle("Marvel");
				movie.setCategory("movie in theater");
				movie.setYear(2021);
				MoviesinTheatres movt = new MoviesinTheatres();
				// add
				System.out.println(movt.add(movie) + "added into database");
				break;

			case 4:
				// adding into topRatedindia list
				movie.setId(4);
				movie.setTitle("Doctor");
				movie.setCategory("TopRatedIndia");
				movie.setYear(2021);
				TopRatedIndia ti = new TopRatedIndia();
				// add
				System.out.println(ti.add(movie) + "added into database");
				break;

			case 5:
				// adding into topratedMovie list
				movie.setId(4);
				movie.setTitle("eternals");
				movie.setCategory("TopRatedIndia");
				movie.setYear(2021);
				TopRatedMovies tM = new TopRatedMovies();
				// add
				System.out.println(tM.add(movie) + "added into database");

				break;

			case 6:
				System.exit(1);
				break;

			}
		}

	}

}

