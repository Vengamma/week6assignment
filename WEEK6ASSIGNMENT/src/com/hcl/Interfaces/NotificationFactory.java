package com.hcl.Interfaces;

public class NotificationFactory {
	public Notification createNotification(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		if ("moviescoming".equals(channel)) {
			return new MovieComing();
		} else if ("moviesinTheater".equals(channel)) {
			return new MoviesinTheatres();
		} else if ("TopRatedIndian".equals(channel)) {
			return new TopRatedIndia();
		} else if ("topRatedMovies".equals(channel)) {
			return new TopRatedMovies();
		}

		return null;

	}
}


