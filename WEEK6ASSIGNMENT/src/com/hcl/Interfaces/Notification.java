package com.hcl.Interfaces;
import java.sql.SQLException;
import java.util.List;

import com.hcl.Model.Movie;
public interface Notification {
	public List<Movie> getMovies() throws SQLException;

	public int add(Movie movie) throws SQLException;
}
