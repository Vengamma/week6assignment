package com.hcl.Interfaces;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.Model.Movie;
import com.hcl.jdbc.DatabaseConnection;

public class MoviesinTheatres implements Notification{
	static Connection con = DatabaseConnection.getConnection();

	public List<Movie> getMovies() throws SQLException {
		// TODO Auto-generated method stub
		String query = "select * from Movies_inTheatres";

		PreparedStatement ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		List<Movie> ls = new ArrayList();

		while (rs.next()) {
			Movie emp = new Movie();
			emp.setId(rs.getInt("id"));
			emp.setTitle(rs.getString("title"));
			emp.setCategory(rs.getString("category"));
			emp.setYear(rs.getInt("year"));
			ls.add(emp);
		}
		return ls;
	}

	
	public int add(Movie movie) throws SQLException {
		String query = "insert into Movies_inTheatres (ID, " + "Title," + "Category," + "year) VALUES (?, ?,?,?)";
		PreparedStatement ps = con.prepareStatement(query);
		ps.setInt(1, movie.getId());
		ps.setString(2, movie.getTitle());
		ps.setString(3, movie.getCategory());
		ps.setInt(4, movie.getYear());
		int n = ps.executeUpdate();
		return n;
	}
}

